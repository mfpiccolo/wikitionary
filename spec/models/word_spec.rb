require 'spec_helper'

describe Word do 
  context 'validations' do 
    it {should validate_presence_of :term}
    it {should validate_presence_of :definition}
  end

  context 'assignment' do 
    it {should allow_mass_assignment_of :term}
    it {should allow_mass_assignment_of :definition}
  end
end