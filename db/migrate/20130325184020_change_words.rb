class ChangeWords < ActiveRecord::Migration
  def  change
    change_table :words do |t|
      t.rename :word, :term
    end
  end
end
